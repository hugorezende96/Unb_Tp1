/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

int main(int argc, char** argv)
{
  Ball ball ;
  const double dt = 1.0/100 ;
  run (Simulation & s, dt){
	for(int i = 0 ; i < 100 ; i++ ) {
		s.step(dt); s.display();}
}
  return 0 ;
}
