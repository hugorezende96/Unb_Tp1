#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{
	private:
		Figure figure;

	public:
		SpringMassDrawable()
		:SpringMass(), figure("SpringMass")
		{
			figure.addDrawable(this);
		}

		void draw()
		{
			double Mass1x, Mass1y, radius1; 
			double Mass2x, Mass2y, radius2;
			double Mass3x, Mass3y, radius3;      // ATRIBUTOS PARA OS OBJETOS
			double Mass4x, Mass4y, radius4;
			double tickness1 = 2, tickness2 = 1;
			
			Mass1x = this->masses[0].getPosition().x;
			Mass1y = this->masses[0].getPosition().y;
			Mass2x = this->masses[1].getPosition().x;
			Mass2y = this->masses[1].getPosition().y;
			Mass3x = this->masses[2].getPosition().x;  // PREENCHE OS ATRIBUTOS 
			Mass3y = this->masses[2].getPosition().y;
			Mass4x = this->masses[3].getPosition().x;
			Mass4y = this->masses[3].getPosition().y;
			radius1 = this->masses[0].getRadius();
			radius2 = this->masses[1].getRadius();
			radius3 = this->masses[2].getRadius();
			radius4 = this->masses[3].getRadius();
			
			figure.drawCircle(Mass1x, Mass1y, radius1);
			figure.drawCircle(Mass2x, Mass2y, radius2);
			figure.drawCircle(Mass3x, Mass3y, radius3);   // DESENHA AS FIGURAS
			figure.drawCircle(Mass4x, Mass4y, radius4);
			figure.drawLine(Mass1x, Mass1y, Mass2x, Mass2y, tickness1);
			figure.drawLine(Mass3x, Mass3y, Mass4x, Mass4y, tickness2);
		}

		void display()
		{
			figure.update();
		}
} ;

int main(int argc, char** argv)
{
  	glutInit(&argc,argv) ;
  	SpringMassDrawable springmass;

	const double Mass1 = 0.1;
	const double Mass2 = 0.3;
	//const double Mass3 = 0.2;
	//const double Mass4 = 0.2; 
	const double radius1 = 0.1;
	const double radius2 = 0.15;     //ATRIBUTOS
	//const double radius3 = 0.20;
	//const double radius4 = 0.20;
	const double naturalLength = 1.00 ;
	const double dt = 1.0/160.0 ;

	Mass m1(Vector2(-.5,0), Vector2(), Mass1, radius1) ;
	Mass m2(Vector2(+.2,0.4), Vector2(), Mass2, radius2) ;  // INSTANCIANDO AS MASSAS
	//Mass m3(Vector2(-.7,0), Vector2(), Mass3, radius3) ;
	//Mass m4(Vector2(+.7,0), Vector2(), Mass4, radius4) ;
	
	springmass.addMass(m1);
	springmass.addMass(m2);
	springmass.addSpring(0,1, naturalLength, 1.0);		// INSTANCIANDO A SPRINGMASS ADD AS MASSAS

	//springmass.addMass(m3);
	//springmass.addMass(m4);
	//springmass.addSpring(0,15, naturalLength, 1.0);

	run(&springmass, dt);
	return 0 ;
}