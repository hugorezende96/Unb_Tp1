<h1> Projeto de TP1 </h1>

<h4>Primeira parte : Boucing Ball</h4>
<p>"Bola Quicando"</p>
 
<h4>O que esse Projeto Visa ? </h4>
<p>Simular o movimento 2D de uma bola ao cair sobre um plano e "Quicando" sendo o tipo de colisão elástica. </p>

<h4>Instrumentos :</h4>
	
<p>	Sublime text, GNU G++ compiler e as bibliotecas 'iostream' e 'stdio',além das classes disponibilzadas no Git.

	</p>

<h4>Complilação e Execução</h4>
<p>	A diretiva de compilação e execução utilizada foi "g++ ball.cpp ball.h test-ball.cpp > saida.txt" pois o sistema que utilizei era o linux
<br> já para a execução o comando usado foi './a.out'</p>

<h4>Descrição das Classes/Arquivos : </h4>

<p>	Ball.h : Define a bola com seus métodos e atributos específicos.<br>
	
	Simulation.h : Classe interface que define como o movimento acontece juntando a Bola e o sistema com suas cordenadas e métodos.<br>
	
	test-ball.cpp : Define o sistema como o intervalo de tempo "dt" e os métodos da ball.h que dizem como ela "quica".<br>
	 Ball.cpp : Implementa os métodos parar criar a bola através dos construtores.<br>
</p>

<h4>Exemplo de Saída do programa :  </h4>

<p>	    

		Nota-se que a cordenada Y representada pela segunda coluna esta diminuindo o que representa a queda, lembrando que o 'solo' está
		na coordenada Y = -1. <br><br> 
		
		0.003 -0.00149<br>
		0.006 -0.00396 <br>
		0.009 -0.00741 <br>
		0.012 -0.01184 <br>
		0.015 -0.01725 <br>
		0.018 -0.02364 <br>
		0.021 -0.03101 <br>
		0.024 -0.03936 <br>
		0.027 -0.04869 <br>
		0.03 -0.059    <br>
		0.033 -0.07029 <br>
		0.036 -0.08256 <br>
		0.039 -0.09581 <br>
		0.042 -0.11004 <br>
		0.045 -0.12525 <br>
		0.048 -0.14144 <br>
		0.051 -0.15861 <br>
		0.054 -0.17676 <br>
		0.057 -0.19589 <br>
		0.06 -0.216    <br>
		
</p>		
<h4> Imagem gerada pelo output atravez do GNUPLOT :</h4>

![Grafico1](Diagrama1.png)

<br>
<h4>Diagrama de Classes:</h4>
<br>
![DiagramaBall](Diagrama%20Ball.png)

<br><br><br>

<h1>Parte 2</h1>

<br>

<h2> Spring-Mass</h2>
<p>"Massa-mola"</p>

<h4>O que esse Projeto Visa ? </h4>


<p>Para esta parte do projeto, a tentativa é simular duas massas que saltam, em queda livre, sendo ambas conectadas por uma mola, que tem uma força para se contrair, aproximando-as ou expandindo, tornando as massas mais afastadas. 


<h4>Instrumentos adicionais:</h4>

<p> simulation.h, springmass.h, springmass.cpp, test-springmass.cpp.

<h4>Compilação e Execução:</h4>
<p>
A diretiva usada foi: g ++ simulation.h springmass.h springmass.cpp test-spring.cpp
<br>
Para a execução ,assim como no primeiro programa, foi usado o comando: ./a.out </p>

<h4>Descrição das Classes/Arquivos:</h4>

<p>
Simulation.h : implementa a classe Simulation () por métodos virtuais apenas, é responsável ​​por servir  de interface entre os programas.<br>

Springmass.h : Declara as principais classes do programa, como as classes : Spring, SpringMass,Vector2,Massa.<br>

Springmass.cpp : implementa os métodos declarados em springmass.h como o método Mass :: step (dt, gravidade) que recebe o tempo e a força da gravidade para calcular a posição, velocidade e aceleração da massa. E o método SpringMass :: step (dt) que utiliza Mass e Spring para calcular a energia e a força do Sistema.<br>

Test-springmass.cpp : cria os objetos mass1 e mass2, com uma mola de conexão, e calcula o movimento das bolas enquanto estão em queda livre.<br>
</p>
<h4> Diagrama de classes:</h4>
<br>
![Diagrama2](DiagramaClasse2.png)

<br>
<h4>Exemplo de saida do programa:</h4>
<p>
(-0.492284,-0.0025)(0.492284,-0.0025)$0.984568 en:0.190129<br>
(-0.469784,-0.01)(0.469784,-0.01)$0.939567 en:0.190892<br>
(-0.434693,-0.0225)(0.434693,-0.0225)$0.869385 en:0.192607<br>
(-0.390846,-0.04)(0.390846,-0.04)$0.781692 en:0.19517<br>
(-0.343311,-0.0625)(0.343311,-0.0625)$0.686621 en:0.198078<br>
(-0.29781,-0.09)(0.29781,-0.09)$0.59562 en:0.200647<br>
(-0.260041,-0.1225)(0.260041,-0.1225)$0.520082 en:0.20233<br>
(-0.234973,-0.16)(0.234973,-0.16)$0.469945 en:0.203<br>
(-0.226201,-0.2025)(0.226201,-0.2025)$0.452402 en:0.203042<br>
(-0.235453,-0.25)(0.235453,-0.25)$0.470907 en:0.203213<br>
(-0.262299,-0.3025)(0.262299,-0.3025)$0.524598 en:0.204298<br>
(-0.304119,-0.36)(0.304119,-0.36)$0.608238 en:0.206734<br>
(-0.356342,-0.4225)(0.356342,-0.4225)$0.712684 en:0.21037<br>
(-0.412932,-0.49)(0.412932,-0.49)$0.825863 en:0.21449<br>
(-0.467074,-0.5625)(0.467074,-0.5625)$0.934147 en:0.218126<br>
(-0.511988,-0.64)(0.511988,-0.64)$1.02398 en:0.220506<br>
(-0.541764,-0.7225)(0.541764,-0.7225)$1.08353 en:0.221451<br>
(-0.552129,-0.81)(0.552129,-0.81)$1.10426 en:0.22151<br>
(-0.541035,-0.9025)(0.541035,-0.9025)$1.08207 en:0.221754<br>
(-0.509005,-0.9025)(0.509005,-0.9025)$1.01801 en:0.223298<br>
(-0.459165,-0.81)(0.459165,-0.81)$0.918331 en:0.226758<br>
(-0.396966,-0.7225)(0.396966,-0.7225)$0.793933 en:0.231914<br>
(-0.329598,-0.64)(0.329598,-0.64)$0.659196 en:0.237753<br>
(-0.265174,-0.5625)(0.265174,-0.5625)$0.530347 en:0.2429<br>
(-0.211763,-0.49)(0.211763,-0.49)$0.423525 en:0.246265<br>
(-0.176395,-0.4225)(0.176395,-0.4225)$0.352789 en:0.247597<br>
(-0.164148,-0.36)(0.164148,-0.36)$0.328297 en:0.247678<br>
<br>
</p>


<h4> Gráfico geraddo pela saída:</h4>
<br>
![Grafico2](grafico2.png)
<br>


<h1>Parte 3</h1>

<p>A terceira parte complementa a segunda de forma que a terceira representa a animação grafica referente ao sistema massa mola da Parte 2<p> 

<h4>Complilação e Execução</h4>
<p>	A diretiva de compilação e execução utilizada  para a parte 3 foi "g++ springmass.cpp graphics.cpp test-springmass-graphics.cpp -o Grafico3" 
lembrando que para executar a parte gráfica é necessário instalar a biblioteca gráfica OPEN/GL e o cmake já para a execução o comando usado foi './Grafico3'</p>

<h4>Descrição das Classes/Arquivos : </h4>

<p>	Graphics.h : Define os métodos utilizados pela biblioteca GLUT<br>
Graphics.cpp : Implementa os métodos utilizados pela biblioteca GLUT<br>
Springmass.cpp : implementa os métodos declarados em springmass.h como o método Mass :: step (dt, gravidade) que recebe o tempo e a força da gravidade para calcular a posição, velocidade e aceleração da massa. E o método SpringMass :: step (dt) que utiliza Mass e Spring para calcular a energia e a força do Sistema.<br>
test-springmass-graphics.cpp : Principal parte do programa pois Implementa a classe SpringMassDrawable que define e 'desenha' as figura e objetos que compõe o sistema massa-mola.
</p>
<br>
<h4>Diagrama De Classes :</h4>
<br>
![Diagramaclass3](DiagramaClasse3.png)
<br>
<h4>Diagrama de Sequencia : </h4>
<br>
![DiagramaSeq](DiagramaSeq.png)
<br>
<h4> Exemplo de Saida Gráfica </h4>
<br>
![Saida3](Graphics.png)
<br>
